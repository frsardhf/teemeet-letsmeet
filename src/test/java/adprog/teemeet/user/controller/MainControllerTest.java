package adprog.teemeet.user.controller;

import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.Errors;

import adprog.teemeet.user.model.User;
import adprog.teemeet.user.model.UserDTO;
import adprog.teemeet.user.service.UserService;
import adprog.teemeet.user.validators.UserDTOValidator;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;


@WebMvcTest(MainController.class)
public class MainControllerTest {
    
    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    @MockBean
    private UserDTOValidator userDTOValidator;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Test
    public void testLandingPageUnauthenticatedReturnsOk() throws Exception {
        mvc.perform(get("/"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("landingPage"))
            .andExpect(view().name("user/landing-page"));
    }

    @WithMockUser(value = "spring")
    @Test
    public void testLandingPageAuthenticatedRedirectsToDashboard() throws Exception {
        mvc.perform(get("/"))
            .andExpect(status().isTemporaryRedirect())
            .andExpect(redirectedUrl("/dashboard"));
    }

    @Test
    public void testDashboardUnauthenticatedRedirectsToSignIn() throws Exception {
        mvc.perform(get("/dashboard"))
            .andExpect(status().isFound());
    }

    @WithMockUser(value = "spring")
    @Test
    public void testLandingPageAuthenticatedReturnsOk() throws Exception {
        mvc.perform(get("/dashboard"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("dashboard"))
            .andExpect(view().name("user/dashboard"));
    }

    @Test
    public void testSignInUnauthenticatedReturnsOk() throws Exception {
        mvc.perform(get("/signin"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("signIn"))
            .andExpect(view().name("user/signin"));
    }

    @WithMockUser(value = "spring")
    @Test
    public void testSignInAuthenticatedRedirectsToDashboard() throws Exception {
        mvc.perform(get("/signin"))
            .andExpect(status().isTemporaryRedirect())
            .andExpect(redirectedUrl("/dashboard"));
    }

    @Test
    public void testSignUpUnauthenticatedReturnsOk() throws Exception {
        when(userDTOValidator.supports(UserDTO.class)).thenReturn(true);
        mvc.perform(get("/signup"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("signUp"))
            .andExpect(view().name("user/signup"));
    }

    @WithMockUser(value = "spring")
    @Test
    public void testSignUpAuthenticatedRedirectsToDashboard() throws Exception {
        when(userDTOValidator.supports(UserDTO.class)).thenReturn(true);
        mvc.perform(get("/signup"))
            .andExpect(status().isTemporaryRedirect())
            .andExpect(redirectedUrl("/dashboard"));
    }

    @Test
    public void testPostSignUpUnauthenticatedRedirectsToSuccess() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("spring");
        userDTO.setEmail("spring@home.com");
        userDTO.setPassword("spring");
        userDTO.setMatchingPassword("spring");

        String encryptedPassword = bCryptPasswordEncoder.encode("spring");

        User user = new User();
        user.setUsername("spring");
        user.setPassword(encryptedPassword);
        user.setEmail("spring@home.com");
        user.setEnabled(true);

        when(userService.signUpUser(userDTO)).thenReturn(user);
        when(userDTOValidator.supports(UserDTO.class)).thenReturn(true);

        mvc.perform(post("/signup")
            .param("username", "spring")
            .param("email", "spring@home.com")
            .param("password", "spring")
            .param("matchingPassword", "spring")
            .with(csrf()))
            .andExpect(status().isFound())
            .andExpect(redirectedUrl("/signup/success"));
    }

    @Test
    public void testPostSignUpUnauthenticatedReturnsInvalidData() throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("spring");
        userDTO.setEmail("spring@home.com");
        userDTO.setPassword("spring");
        userDTO.setMatchingPassword("spring123");

        String encryptedPassword = bCryptPasswordEncoder.encode("spring");

        User user = new User();
        user.setUsername("spring");
        user.setPassword(encryptedPassword);
        user.setEmail("spring@home.com");
        user.setEnabled(true);

        when(userService.signUpUser(userDTO)).thenReturn(user);
        when(userDTOValidator.supports(UserDTO.class)).thenReturn(true);
        doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Errors errors = (Errors)args[1];
                errors.rejectValue("matchingPassword", "Match.appUserForm.confirmPassword");
                return null;
              }
        }).when(userDTOValidator).validate(any(UserDTO.class), any(Errors.class));

        mvc.perform(post("/signup")
            .param("username", "spring")
            .param("email", "spring@home.com")
            .param("password", "spring")
            .param("matchingPassword", "spring123")
            .with(csrf()))
            .andExpect(model().hasErrors())
            .andExpect(model().attributeHasFieldErrorCode("user", "matchingPassword", "Match.appUserForm.confirmPassword"))
            .andExpect(view().name("user/signup"));
    }

}
