package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.core.CalendarSlots;
import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.repository.MeetEventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MeetEventServiceImplTest {
    @Mock
    private MeetEventRepository meetEventRepository;

    @InjectMocks
    private MeetEventServiceImpl meetEventService;

    private LetsMeetEvent letsMeetEvent;
    private CalendarSlots calendarSlots;

    @BeforeEach
    public void setUp(){
        letsMeetEvent = new LetsMeetEvent();
        letsMeetEvent.setId(Long.parseLong("135135"));
        letsMeetEvent.setName("Group Project Microservice");
        letsMeetEvent.setDate(LocalDate.parse("2021-05-21"));
        String timePattern = "HH-mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        letsMeetEvent.setStartTime(LocalTime.parse("08-00", timeFormatter));
        letsMeetEvent.setEndTime(LocalTime.parse("13-00", timeFormatter));
    }

    @Test
    public void testServiceCreateEvent(){
        lenient().when(meetEventService.createMeetEvent(letsMeetEvent)).thenReturn(letsMeetEvent);
    }

    @Test
    public void testServiceGetEventById(){
        Optional<LetsMeetEvent> event = Optional.of(letsMeetEvent);
        when(meetEventRepository.findById(letsMeetEvent.getId()))
                .thenReturn(event);
        LetsMeetEvent eventResult = meetEventService.getMeetEventById(letsMeetEvent.getId());
        List<List<String>> partitioned = meetEventService.getPartitionedMeetEventTimeSlots(eventResult);
        assertEquals(letsMeetEvent, eventResult);
        assertEquals(meetEventService.getPartitionedMeetEventTimeSlots(letsMeetEvent).size(), partitioned.size());
    }

    @Test
    public void testServiceUpdateEvent(){
        meetEventService.createMeetEvent(letsMeetEvent);
        LocalTime currentStart = letsMeetEvent.getStartTime();
        LocalTime currentEnd = letsMeetEvent.getEndTime();
        String start = "12-00";
        String end = "16-00";
        String timePattern = "HH-mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        letsMeetEvent.setStartTime(LocalTime.parse("12-00", timeFormatter));
        letsMeetEvent.setEndTime(LocalTime.parse("16-00", timeFormatter));

        lenient().when(meetEventService.updateEvent(start, end, letsMeetEvent))
                .thenReturn(letsMeetEvent);
        LetsMeetEvent resultEvent = meetEventService.updateEvent(start, end, letsMeetEvent);

        assertNotEquals(resultEvent.getStartTime(), currentStart);
        assertNotEquals(resultEvent.getEndTime(), currentEnd);
    }
}
