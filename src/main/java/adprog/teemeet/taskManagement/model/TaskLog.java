package adprog.teemeet.taskManagement.model;
import adprog.teemeet.taskManagement.core.status.CompleteStatus;
import adprog.teemeet.taskManagement.core.status.InProgressStatus;
import adprog.teemeet.taskManagement.core.status.IncompleteStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tasklog")
@Data
@NoArgsConstructor
public class TaskLog {

    @Id
    @Column(name="idTask")
    private Long idTask;

    @Column(name="judul")
    private String judulTask;

    @Column(name="percent")
    private int percent;

    @Column(name="status")
    private String status;

    @ManyToMany(mappedBy = "tasks")
    private List<PersonLog> assignne;

    @ManyToOne
    @JoinColumn(name="task_group")
    private GroupLog task_group;

    public TaskLog(Long id,String judulTask, List<PersonLog> assignne){
        this.idTask = id;
        this.judulTask = judulTask;
        this.percent = 0;
        this.assignne = assignne;
        this.status = "";

    }
    @Autowired
    public void setPercent(int percent) {
        this.percent = percent;
        setStatusPercent(percent);
    }

    private void setStatusPercent(int percent){
        CompleteStatus completeStatus = new CompleteStatus();
        IncompleteStatus incompleteStatus = new IncompleteStatus();
        InProgressStatus inProgressStatus = new InProgressStatus();
        if(percent == 0){
            this.status = incompleteStatus.showStatus();
        }else if(percent<100){
            status = inProgressStatus.showStatus();
        }else{
            status = completeStatus.showStatus();
        }
    }
}
