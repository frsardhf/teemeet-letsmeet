package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.GroupRepository;
import adprog.teemeet.taskManagement.repository.PersonRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PersonServiceImpl implements PersonService{
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public PersonLog createPerson(String number, String nama, String role) {
        PersonLog person = new PersonLog(number, nama, role);
        personRepository.save(person);
        return person;
    }

    @Override
    public PersonLog getPerson(String number) {
        return personRepository.findByNumber(number);
    }

    @Override
    public PersonLog updatePerson(PersonLog person, String number) {
        String role = person.getRole();
        String nama = person.getNama();
        personRepository.findByNumber(number).setRole(role);
        personRepository.findByNumber(number).setNama(nama);
        personRepository.save(person);
        return person;
    }

    @Override
    public void deletePerson(String number) {
        PersonLog person = personRepository.findByNumber(number);
        personRepository.delete(person);
    }

    @Override
    public PersonLog addGroupHasJoin(String number,String kodeGroup) {
        PersonLog person = personRepository.findByNumber(number);
        List<GroupLog> groupJoin = person.getGroups();
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        groupJoin.add(group);
        person.setGroups(groupJoin);
        personRepository.save(person);
        return person;
    }

    @Override
    public Iterable<GroupLog> getGroupJoin(String number) {
        PersonLog person = personRepository.findByNumber(number);
        return person.getGroups();
    }

    @Override
    public Iterable<TaskLog> getPersonalTask(String number) {
        PersonLog person = personRepository.findByNumber(number);
        return person.getTasks();
    }
}
