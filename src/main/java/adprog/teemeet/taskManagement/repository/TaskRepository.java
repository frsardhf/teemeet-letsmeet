package adprog.teemeet.taskManagement.repository;

import adprog.teemeet.taskManagement.model.TaskLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface TaskRepository extends JpaRepository<TaskLog, Long> {
    TaskLog findByIdTask(Long idTask);

}
