package adprog.teemeet.letsmeet.repository;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EventRepositoryTest {
/*
    @Autowired
    private EventRepository eventRepository;

    @Test
    public void whenFindByNameExist() {
        Long id = Long.valueOf("1231242414");
        String date = "2021-05-06";
        String startTime = "09-00";
        String endTime = "11-00";
        eventRepository.save(new LetsMeetEvent(id, "Event a", date, startTime, endTime));
        assertNotNull(eventRepository.findById(id));
    }

    @Test
    public void whenFindByNameNotExist() {
        Long id1 = Long.valueOf("1231242414");
        Long id2 = Long.valueOf("1231242415");
        String date = "2021-05-06";
        String startTime = "09-00";
        String endTime = "11-00";
        eventRepository.save(new LetsMeetEvent(id1, "Event a", date, startTime, endTime));
        assertNull(eventRepository.findById(id2));
    }

 */
}
