package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.GroupRepository;
import adprog.teemeet.taskManagement.repository.PersonRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class GroupServiceImplTest {
    @Mock
    private GroupRepository groupRepository;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    private GroupServiceImpl groupService;

    @InjectMocks
    private TaskServiceImpl taskService;

    @InjectMocks
    private PersonServiceImpl personService;


    private GroupLog group;
    private PersonLog person;

    @BeforeEach
    void setUp() {
        group = new GroupLog();
        group.setNama("ters");
        group.setKodeGroup("12345");

        person = new PersonLog();
        person.setNama("budi");
        person.setRole("leader");
        person.setNumber("1");
    }

    /*
    @Test
    void createGroup() {
        lenient().when(groupService.createGroup("ters","12345")).thenReturn(group);
    }

     */

    @Test
    void testgetGroup() {
        lenient().when(groupService.getGroup("12345")).thenReturn(group);
        GroupLog resultGroup = groupService.getGroup(group.getKodeGroup());
        assertEquals(group.getKodeGroup(),resultGroup.getKodeGroup());
        //System.out.println(groupService.getGroup(group.getKodeGroup()));
    }

    /*
    @Test
    void setNamaGroup() {
        lenient().when(groupService.setNamaGroup("qnq",group)).thenReturn(group);
        assertEquals(group.getNama(),"qnq");
        //groupService.setNamaGroup("ters",group);
    }

    /*
    @Test
    void deleteGroup() {
        groupService.createGroup("ters","12");
>>>>>>> 1e1ce9797dd7e1ad0f9e55ef5fe2d8bfaa16fed6
        groupService.deleteGroup("12");
        lenient().when(groupService.getGroup("12")).thenReturn(null);
        assertEquals(null,groupService.getGroup("12"));
    }

<<<<<<< HEAD

=======
     */


    @Test
    void getListTask() {
        //groupService.createGroup("bruh","99");
        //Iterable<TaskLog> tasks = groupService.getGroup("12345").getGroup_tasks();
        //System.out.println(group.getGroup_tasks());
        //lenient().when(groupService.getListTask("12345")).thenReturn(tasks);
        //Iterable<TaskLog> listTask = group.getGroup_tasks();
        //Assertions.assertIterableEquals(listMahasiswa, listMahasiswaResult);

    }

    @Test
    void addTaskGroup() {

    }

    @Test
    void deleteTaskGroup() {
    }

    @Test
    void getMember() {
    }

    @Test
    void addMember() {
    }

    @Test
    void removeMember() {
    }

    @Test
    void notifyTask() {
    }
}