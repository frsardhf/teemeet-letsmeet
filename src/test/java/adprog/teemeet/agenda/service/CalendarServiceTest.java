package adprog.teemeet.agenda.service;

import adprog.teemeet.agenda.model.Event;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class CalendarServiceTest {

    private CalendarServiceImpl agendaService;

    Event event;

    @BeforeEach
    public void setUp() {
        agendaService = new CalendarServiceImpl();
    }

    @Test
    void testAgendaServiceGetDatesinAMonthReturnValue() throws Exception {
        List res = agendaService.getDaysinAMonth(2021, 4);
        assertEquals(1,res.get(4));
        assertFalse(res.contains(31));
    }
    /*

     */
}
