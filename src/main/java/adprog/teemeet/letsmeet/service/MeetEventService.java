package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;

import java.util.List;

public interface MeetEventService {
    LetsMeetEvent createMeetEvent(LetsMeetEvent event);

    LetsMeetEvent getMeetEventById(Long id);

    LetsMeetEvent updateEvent(String start, String end, LetsMeetEvent event);

    List<List<String>> getPartitionedMeetEventTimeSlots(LetsMeetEvent event);
}
