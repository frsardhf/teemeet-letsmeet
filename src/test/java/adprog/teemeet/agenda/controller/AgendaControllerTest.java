package adprog.teemeet.agenda.controller;

import adprog.teemeet.agenda.service.EventServiceImpl;
import adprog.teemeet.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AgendaController.class)
class AgendaControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private EventServiceImpl eventService;

    @WithMockUser(value = "spring")
    @Test
    void whenCalendarURLIsAccessedItShouldContainDates() throws Exception {
        mockMvc.perform(get("/agenda/calendar?month=4&year=2021"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("agendaLanding"))
                .andExpect(model().attributeExists("days"))
                .andExpect(model().attributeExists("month"))
                .andExpect(model().attributeExists("year"))
                .andExpect(view().name("agenda/agendaCalendar"));
        verify(eventService, times(1)).getDayList(2021,4);
    }

    @WithMockUser(value = "spring")
    @Test
    void whenAgendaURLIsAccessedItShouldRedirect() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        mockMvc.perform(get("/agenda"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("agenda"))
                .andExpect(redirectedUrl("/agenda/calendar?month="+now.getMonthValue()+"&year="+now.getYear()));
    }

    @WithMockUser(value = "spring")
    @Test
    void whenPrevURLIsAccessedItShouldRedirect() throws Exception {
        mockMvc.perform(get("/agenda/calendar/prev?month=1&year=2021"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("agendaPrevMonth"))
                .andExpect(redirectedUrl("/agenda/calendar?month=12&year=2020"));
    }

    @WithMockUser(value = "spring")
    @Test
    void whenNextURLIsAccessedItShouldRedirect() throws Exception {
        mockMvc.perform(get("/agenda/calendar/next?month=12&year=2020"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("agendaNextMonth"))
                .andExpect(redirectedUrl("/agenda/calendar?month=1&year=2021"));
    }

    @WithMockUser(value = "spring")
    @Test
    void whenEventURLIsAccessedItShouldContainEvents() throws Exception {
        mockMvc.perform(get("/agenda/events?month=4&year=2021&day=10"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("dailyEvents"))
                .andExpect(model().attributeExists("day"))
                .andExpect(model().attributeExists("month"))
                .andExpect(model().attributeExists("year"))
                .andExpect(model().attributeExists("events"))
                .andExpect(view().name("agenda/agendaDailyEvents"));
        verify(eventService, times(1)).getEventByYearMonthDay(2021,4,10);
    }

    @WithMockUser(value = "spring")
    @Test
    void whenCreateEventURLIsAccessedItShouldLoadCreateEventHtml() throws Exception {
        mockMvc.perform(get("/agenda/events/create?month=4&year=2021&day=10"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("createEventForm"))
                .andExpect(view().name("agenda/createEvent"));
        verify(eventService, times(1)).dateFormatter(2021,4,10);
    }
    /*
    @WithMockUser(value = "spring")
    @Test
    void whenCreateEventFormURLIsAccessedItShouldSetRequestToService() throws Exception {
        mockMvc.perform(post("/agenda/events/create")
                .param("name", "dummy")
                .param("desc", "dummy")
                .param("date", "2021-04-01")
                .param("startTime", "10:00:00")
                .param("endTime", "12:00:00"))
                .andExpect(handler().methodName("createEventSubmit"))
                .andExpect(status().is3xxRedirection());

        verify(eventService, times(1)).createEvent(any(String.class),any(String.class),any(String.class),any(String.class),any(String.class));
    }

     */
}

/*
public class AdapterControllerTest {






    @Test
    public void whenAttackURLIsAccessedItShouldCallAttackWithWeapon() throws Exception {
        mockMvc.perform(post("/battle/attack")
                .param("weaponName", "Ionic Bow")
                .param("attackType", "1"))
                .andExpect(handler().methodName("attackWithWeapon"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/battle"));
        verify(weaponService, times(1)).attackWithWeapon("Ionic Bow", 1);
    }
}

 */