package adprog.teemeet.user.model;

public enum WebRole {
    ADMIN, USER
}
