package adprog.teemeet.agenda.service;

import adprog.teemeet.agenda.core.DailyEvents;
import adprog.teemeet.agenda.model.Event;
import adprog.teemeet.agenda.repository.AgendaEventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EventServiceTest {
    @Mock
    private CalendarServiceImpl calendarService;

    @Mock
    private AgendaEventRepository agendaEventRepository;

    @InjectMocks
    private EventServiceImpl eventService;

    private Event event;

    private Event event1;

    @BeforeEach
    public void setUp() throws Exception {
        event = new Event();
        event.setName("Dummy");
        event.setDescription("Dummy");
        event.setDate(LocalDate.parse("2017-01-13"));
        event.setTimeStart(LocalTime.parse("17:09:42.411"));
        event.setTimeEnd(LocalTime.parse("17:09:50.411"));

        event1 = new Event();
        event1.setName("Dummy2");
        event1.setDescription("Dummy2desc");
        event1.setDate(LocalDate.parse("2017-01-13"));
        event1.setTimeStart(LocalTime.parse("17:09:42.411"));
        event1.setTimeEnd(LocalTime.parse("17:09:50.411"));


    }

    @Test
     void testServiceCreateEvent() throws Exception {
        when(eventService.createEvent("Dummy", "Dummy", "2017-01-13",
                "17:09:42.411", "17:09:50.411")).thenReturn(event);
        assertEquals(event,eventService.createEvent("Dummy", "Dummy", "2017-01-13",
                "17:09:42.411", "17:09:50.411"));
    }

    @Test
    void testGetDayList() throws Exception {
        List<Event> eventList = new ArrayList<>();
        List<Integer> dayList = new ArrayList<>();

        dayList.add(0);
        List<DailyEvents> expected = new ArrayList<>();
        expected.add(new DailyEvents());
        for (int i = 0; i < 31; i++){
            expected.add(new DailyEvents(i, eventList));
            dayList.add(i);
        }
        when(calendarService.getDaysinAMonth(2021,6)).thenReturn(dayList);
        when(agendaEventRepository.getByYearAndMonthAndDay(eq(2021),eq(6),anyInt())).thenReturn(new ArrayList<>());
        List<DailyEvents> result = eventService.getDayList(2021,6);
        assertEquals(expected.get(1).getDay(), result.get(1).getDay());

    }

    @Test
     void testDateFormatter() throws Exception {
        assertEquals(LocalDate.parse("2021-05-07"), eventService.dateFormatter(2021,5,7));
    }
}
