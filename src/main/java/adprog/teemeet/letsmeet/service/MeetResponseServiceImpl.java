package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;
import adprog.teemeet.letsmeet.repository.MeetResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MeetResponseServiceImpl implements MeetResponseService {
    @Autowired
    MeetResponseRepository meetResponseRepository;

    @Autowired
    MeetEventService meetEventService;

    @Override
    public LetsMeetResponse createMeetResponse(Long id,
                                               String username,
                                               List<String> selectedTime,
                                               LetsMeetResponse response) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        response.setEvent(event);
        response.setUsername(username);
        response.setSelectedTime(selectedTime);
        meetResponseRepository.save(response);
        return response;
    }

    @Override
    public void deleteResponse(LetsMeetResponse response) {
        meetResponseRepository.delete(response);
    }

    @Override
    public Iterable<LetsMeetResponse> getListResponseByEvent(Long id) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        List<LetsMeetResponse> responses = meetResponseRepository.findByEvent(event);
        return responses;
    }

    @Override
    public void deleteAllResponse(Long id) {
        Iterable<LetsMeetResponse> responses = getListResponseByEvent(id);
        for (LetsMeetResponse response : responses) {
            deleteResponse(response);
        }
    }
}
