package adprog.teemeet.user.model;

public enum UserRole {
    ADMIN, USER
}
