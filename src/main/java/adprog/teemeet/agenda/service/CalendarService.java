package adprog.teemeet.agenda.service;

import java.util.List;

public interface CalendarService {
    List<Integer> getDaysinAMonth(int month, int year);
}
