package adprog.teemeet.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import adprog.teemeet.user.model.UserDTO;
import adprog.teemeet.user.service.UserService;
import adprog.teemeet.user.validators.UserDTOValidator;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDTOValidator userDTOValidator;

    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {
        // Form target
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }

        if (target.getClass() == UserDTO.class) {
            dataBinder.setValidator(userDTOValidator);
        }
    }
    
    @GetMapping("/")
    public String landingPage(){
        return "user/landing-page";
    }

    @GetMapping("/dashboard")
    public String dashboard() {
        return "user/dashboard";
    }
 
    @GetMapping("/signin")  
    public String signIn(Model model, @RequestParam(required = false) boolean error) {  
        model.addAttribute("error", error);
        return "user/signin";
    }   

    @GetMapping("/signup")  
    public String signUp(Model model) {  
        UserDTO userDTO = new UserDTO();
        model.addAttribute("user", userDTO);
        return "user/signup";  
    } 

    // This method is called to save the registration information.
    // @Validated: To ensure that this Form
    // has been Validated before this method is invoked.
    @PostMapping(value = "/signup")
    public String signUp(Model model, 
            @ModelAttribute("user") @Validated UserDTO userDTO, 
            BindingResult result, 
            final RedirectAttributes redirectAttributes) {
    
        // Validate result
        if (result.hasErrors()) {
            return "user/signup";
        }

        try {
            userService.signUpUser(userDTO);
        }
        // Any unknown errors (shouldn't be happened)
        catch (Exception e) {
            model.addAttribute("errorMessage", "Error: " + e.getMessage());
            return "user/signup";
        }
        
        return "redirect:/signup/success";
    }

    @GetMapping(value = "/signup/success")
    public String signUpSuccess() {
        return "user/success";
    }
}
