package adprog.teemeet.user.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import adprog.teemeet.user.model.User;
import adprog.teemeet.user.model.UserDTO;
import adprog.teemeet.user.model.WebRole;
import adprog.teemeet.user.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    
    @InjectMocks
    private UserService userService;

    @Mock
	private UserRepository userRepository;

	@Mock
	private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Test
    public void testLoadUserByUsernameNotFound() throws Exception {
        when(userRepository.findByUsername("spring")).thenReturn(Optional.ofNullable(null));
        assertThrows(UsernameNotFoundException.class, () -> userService.loadUserByUsername("spring"));
    }

    @Test
    public void testLoadUserByUsernameFound() throws Exception {
        User user = new User();
        user.setUsername("spring");
        user.setEmail("spring@home.com");
        user.setPassword("spring");
        user.setEnabled(true);

        when(userRepository.findByUsername("spring")).thenReturn(Optional.ofNullable(user));

        UserDetails userReturned = userService.loadUserByUsername("spring");
        assertEquals(user, userReturned);
    }

    @Test
    public void testSignUpUser() throws Exception {
        User user = new User();
        user.setUsername("spring");
        user.setEmail("spring@home.com");
        user.setPassword("spring");
        user.setEnabled(true);

        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("spring");
        userDTO.setEmail("spring@home.com");
        userDTO.setPassword("spring");
        userDTO.setMatchingPassword("spring");

        when(userRepository.save(any(User.class))).thenReturn(user);
        when(bCryptPasswordEncoder.encode("spring")).thenReturn("spring");

        User userReturned = userService.signUpUser(userDTO);

        assertEquals(userDTO.getUsername(), userReturned.getUsername());
        assertEquals(userDTO.getEmail(), userReturned.getEmail());
        assertEquals(userDTO.getPassword(), userReturned.getPassword());
        assertTrue(userReturned.isEnabled());
        assertTrue(userReturned.isCredentialsNonExpired());
        assertTrue(userReturned.isAccountNonExpired());
        assertTrue(userReturned.isAccountNonLocked());
        assertEquals(Collections.singletonList(new SimpleGrantedAuthority(WebRole.USER.name())), userReturned.getAuthorities());
    }

}
