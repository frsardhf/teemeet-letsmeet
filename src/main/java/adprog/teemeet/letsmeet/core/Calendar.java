package adprog.teemeet.letsmeet.core;

import java.time.LocalTime;

/**
 * Interface representing Calendar
 */
public interface Calendar {
    LocalTime getStartTime();
    
    LocalTime getEndTime();
}
