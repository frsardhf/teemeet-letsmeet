package adprog.teemeet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;  
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;  
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import adprog.teemeet.user.filters.AuthenticatedFilter;
import adprog.teemeet.user.service.UserService;  

@Configuration  
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {  

    @Autowired
    private UserService userService;

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {

		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
			.authorizeRequests()
					.antMatchers("/signup", "/signin", "/signup/success", "/").permitAll()
					.anyRequest().authenticated()
					.and()
				.formLogin()
					.loginPage("/signin")
						.defaultSuccessUrl("/dashboard", false)
						.failureUrl("/signin?error=true")
					.permitAll()
					.and()
				.logout()
					.logoutUrl("/signout")
					.logoutSuccessUrl("/")
					.permitAll();

		http.addFilterAfter(new AuthenticatedFilter(), UsernamePasswordAuthenticationFilter.class).authorizeRequests();

	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService)
				.passwordEncoder(bCryptPasswordEncoder());
	}


}