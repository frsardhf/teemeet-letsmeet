package adprog.teemeet.agenda.service;

import adprog.teemeet.agenda.core.Calendar;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalendarServiceImpl implements CalendarService {
    private Calendar calendar = Calendar.getInstance();

    @Override
    public List<Integer> getDaysinAMonth(int year, int month) {
        return calendar.getDatesinAMonth(year, month);
    }

}
