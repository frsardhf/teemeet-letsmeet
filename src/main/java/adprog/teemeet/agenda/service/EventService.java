package adprog.teemeet.agenda.service;

import adprog.teemeet.agenda.core.DailyEvents;
import adprog.teemeet.agenda.model.Event;

import java.time.LocalDate;
import java.util.List;

public interface EventService {
    Event createEvent(String name, String desc, String date, String timeStart, String timeEnd);
    Event getEventByEventId(Long eventId);
    List<Event> getEventByYearMonthDay(int year, int month, int day);
    List<DailyEvents> getDayList(int year, int month);
    LocalDate dateFormatter(int year, int month, int day);
    Event updateEvent(Long eventId, String name, String desc, String date, String timeStart, String timeEnd);

}
