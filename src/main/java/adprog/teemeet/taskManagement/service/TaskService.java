package adprog.teemeet.taskManagement.service;


import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;

import java.util.List;


public interface TaskService {
    TaskLog makeTask(String kodeGroup, Long idTask, String judulTask, List<String> number,
                     String name, String description);
    //TaskLog editPersonalTask(Long id,PersonLog person,String judul,int percent, GroupLog group);
    void removeTask(Long id, String kodeGroup);
    TaskLog addAssignee(Long id,PersonLog person);
    Iterable<PersonLog> getAssignee(Long id);
    TaskLog getTask(Long id);
    TaskLog updateTask(Long idTask, int percent,String judulTask, List<String> numberPerson);
}
