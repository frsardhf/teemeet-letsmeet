package adprog.teemeet.agenda.core;

import adprog.teemeet.agenda.model.Event;

import java.util.ArrayList;
import java.util.List;

public class DailyEvents {
    private int day;
    private List<Event> events;

    public DailyEvents(int day, List<Event> events){
        this.day = day;
        this.events = events;
    }

    public DailyEvents(){
        this(0, new ArrayList<>());
    }

    @Override
    public String toString(){
        var stringBuilder = new StringBuilder();
        stringBuilder.append(day + "\n");
        for (Event event: events)
            stringBuilder.append("\n" + event);
        return stringBuilder.toString();
    }

    public int getDay() {
        return day;
    }

    public List<Event> getEvents() {
        return events;
    }
}
