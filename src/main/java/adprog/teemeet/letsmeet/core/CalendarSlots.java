package adprog.teemeet.letsmeet.core;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class CalendarSlots implements Calendar{
    LocalTime startTime;
    LocalTime endTime;

    public ArrayList<String> getTimeSlots(LetsMeetEvent event) {
        ArrayList<String> timeSlots = new ArrayList<String>();
        LocalTime start = event.getStartTime();
        LocalTime end = event.getEndTime();
        for (int i = start.getHour(); i <= end.getHour(); i++) {
            StringBuilder sb = new StringBuilder();
            if (i == end.getHour()) {
                if (i < 10)
                    sb.append("0" + i + "-00");
                else
                    sb.append(i + "-00");
                timeSlots.add(sb.toString());
                break;
            } else if (i < 10) {
                sb.append("0" + i + "-00");
                timeSlots.add(sb.toString());
                sb.replace(3, 5, "30");
                timeSlots.add(sb.toString());
            }
            else {
                sb.append(i + "-00");
                timeSlots.add(sb.toString());
                sb.replace(3, 5, "30");
                timeSlots.add(sb.toString());
            }
        }
        return timeSlots;
    }
    
    public List<List<String>> getPartitionedTimeSlots(ArrayList<String> list) {
        int size = 8;
        List<List<String>> partitions = new ArrayList<>();
        for (int i = 0; i < list.size(); i += size) {
            partitions.add(list.subList(i, Math.min(i + size, list.size())));
        }
        return partitions;
    }

    @Override
    public LocalTime getStartTime() {
        return startTime;
    }

    @Override
    public LocalTime getEndTime() {
        return endTime;
    }
}
