package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.core.CalendarSlots;
import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.repository.MeetEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class MeetEventServiceImpl implements MeetEventService{
    private CalendarSlots calendarSlots;

    @Autowired
    MeetEventRepository meetEventRepository;

    public MeetEventServiceImpl() {
        this.calendarSlots = new CalendarSlots();
    }

    @Override
    public LetsMeetEvent createMeetEvent(LetsMeetEvent event) {
        meetEventRepository.save(event);
        return event;
    }

    @Override
    public LetsMeetEvent getMeetEventById(Long id) {
        return meetEventRepository.findById(id).get();
    }

    @Override
    public LetsMeetEvent updateEvent(String start, String end, LetsMeetEvent event) {
        String timePattern = "HH-mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        event.setStartTime(LocalTime.parse(start, timeFormatter));
        event.setEndTime(LocalTime.parse(end, timeFormatter));
        meetEventRepository.save(event);
        return event;
    }

    @Override
    public List<List<String>> getPartitionedMeetEventTimeSlots(LetsMeetEvent event) {
        return calendarSlots.getPartitionedTimeSlots(calendarSlots.getTimeSlots(event));
    }
}
