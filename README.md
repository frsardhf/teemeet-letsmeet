# TEEMEET<br>
[![pipeline status](https://gitlab.com/NaufalAdi/teemeet/badges/master/pipeline.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits) [![coverage report](https://gitlab.com/NaufalAdi/teemeet/badges/master/coverage.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits)

## Angggota Kelompok:
1. William<br>
   [![pipeline status](https://gitlab.com/NaufalAdi/teemeet/badges/teemeet-hori75/pipeline.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits) [![coverage report](https://gitlab.com/NaufalAdi/teemeet/badges/teemeet-hori75/coverage.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits)

2. Faris Sayidinarechan Ardhafa<br>
   [![pipeline status](https://gitlab.com/NaufalAdi/teemeet/badges/teemeet-ardha/pipeline.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits) [![coverage report](https://gitlab.com/NaufalAdi/teemeet/badges/teemeet-ardha/coverage.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits)

3. Mohammad Riswanda Alifarahman<br>
   [![pipeline status](https://gitlab.com/NaufalAdi/teemeet/badges/teemeet-risw24/pipeline.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits) [![coverage report](https://gitlab.com/NaufalAdi/teemeet/badges/teemeet-risw24/coverage.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits)

4. Naufal Adi Wijanarko<br>
   [![pipeline status](https://gitlab.com/NaufalAdi/teemeet/badges/teemeet-naufal/pipeline.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits) [![coverage report](https://gitlab.com/NaufalAdi/teemeet/badges/teemeet-naufal/coverage.svg)](https://gitlab.com/NaufalAdi/teemeet/-/commits)
   
