package adprog.teemeet.taskManagement.service;

import adprog.teemeet.agenda.model.Event;
import adprog.teemeet.agenda.repository.AgendaEventRepository;
import adprog.teemeet.taskManagement.core.status.CompleteStatus;
import adprog.teemeet.taskManagement.core.status.InProgressStatus;
import adprog.teemeet.taskManagement.core.status.IncompleteStatus;
import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.GroupRepository;
import adprog.teemeet.taskManagement.repository.PersonRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService{
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AgendaEventRepository agendaEventRepository;

    @Override
    public TaskLog makeTask(String kodeGroup, Long idTask, String judulTask, List<String> number, String name, String description){
        //Event event = new Event(name,description,null,null);
        //agendaEventRepository.save(event);
        IncompleteStatus incompleteStatus = new IncompleteStatus();
        List<PersonLog> assignne = new ArrayList<PersonLog>();
        for(String numberPerson : number){
            PersonLog person = personRepository.findByNumber(numberPerson);
            assignne.add(person);
        }
        TaskLog tasks = new TaskLog(idTask,judulTask,assignne);
        tasks.setStatus(incompleteStatus.showStatus());
        for(String numberperson : number){
            PersonLog person = personRepository.findByNumber(numberperson);
            person.getTasks().add(tasks);
            personRepository.save(person);
        }
        groupService.addTaskGroup(kodeGroup,tasks);
        return tasks;
    }
    @Override
    public void removeTask(Long id, String kodeGroup){
        TaskLog task = taskRepository.findByIdTask(id);
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        //GroupServiceImpl groupService = new GroupServiceImpl();
        groupService.deleteTaskGroup(group, task);
        if(taskRepository.findByIdTask(id).getAssignne().size()==0) {
            taskRepository.delete(task);
        }else{
            for(PersonLog assignee : task.getAssignne()){
                assignee.getTasks().remove(task);
            }
            taskRepository.delete(task);
        }

    }
    /*
    @Override
<<<<<<< HEAD
    public TaskLog editPersonalTask(String id,PersonLog person,String judul,int percent, GroupLog group){
=======
    public TaskLog editPersonalTask(Long id,PersonLog person,String judul,int percent, GroupLog group){
>>>>>>> 1e1ce9797dd7e1ad0f9e55ef5fe2d8bfaa16fed6
        GroupServiceImpl groupService = new GroupServiceImpl();
        TaskLog task = taskRepository.findByIdTask(id);
        for(GroupLog groups : person.getGroups()){
            if(groups.equals(group)){
                for(TaskLog taskGroup : groups.getGroup_tasks()){
                    if(taskGroup.equals(task)){
                        taskGroup.setJudulTask(judul);
                        taskGroup.setPercent(percent);
                        taskRepository.save(taskGroup);
                        groupService.notifyTask(taskGroup,group);
                    }
                }
            }
        }
        return task;
    }

<<<<<<< HEAD
    @Override
    public TaskLog addAssignee(String id, PersonLog person) {
=======
     */

    @Override
    public TaskLog updateTask(Long idTask, int percent,
                              String judulTask, List<String> numberPerson) {
        //PersonLog person = personRepository.findByNumber(number);
        TaskLog task = taskRepository.findByIdTask(idTask);
        task.setPercent(percent);
        task.setJudulTask(judulTask);
        for(String numberId : numberPerson){
            PersonLog addPerson = personRepository.findByNumber(numberId);
            task.getAssignne().add(addPerson);
            addPerson.getTasks().add(task);
            personRepository.save(addPerson);
        }
        CompleteStatus completeStatus = new CompleteStatus();
        IncompleteStatus incompleteStatus = new IncompleteStatus();
        InProgressStatus inProgressStatus = new InProgressStatus();
        if(percent == 0){
            task.setStatus(incompleteStatus.showStatus());
        }else if(percent<100){
            task.setStatus(inProgressStatus.showStatus());
        }else{
            task.setStatus(completeStatus.showStatus());
        }
        taskRepository.save(task);
        return task;
    }

    @Override
    public TaskLog addAssignee(Long id, PersonLog person) {
        TaskLog task = taskRepository.findByIdTask(id);
        task.getAssignne().add(person);
        taskRepository.save(task);
        return task;
    }

    @Override
    public Iterable<PersonLog> getAssignee(Long id) {
        TaskLog task = taskRepository.findByIdTask(id);
        return task.getAssignne();
    }

    @Override
    public TaskLog getTask(Long id) {
        return taskRepository.findByIdTask(id);
    }
}
