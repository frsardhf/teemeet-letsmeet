package adprog.teemeet.taskManagement.core.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InProgressStatusTest {
    private InProgressStatus inProgressStatus;

    @BeforeEach
    void setUp() {
        inProgressStatus = new InProgressStatus();
    }

    @Test
    void showStatus() {
        String status = inProgressStatus.showStatus();
        assertEquals("InProgress", status );
    }
}