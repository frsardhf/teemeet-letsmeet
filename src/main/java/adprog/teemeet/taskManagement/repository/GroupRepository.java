package adprog.teemeet.taskManagement.repository;

import adprog.teemeet.taskManagement.model.GroupLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<GroupLog, String> {
    GroupLog findByKodeGroup(String kode);
}
