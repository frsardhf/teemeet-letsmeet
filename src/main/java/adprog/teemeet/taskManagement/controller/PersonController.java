package adprog.teemeet.taskManagement.controller;
import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/user")
public class PersonController {
    @Autowired
    private PersonService personService;

    @PostMapping(path = "/create",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createPerson(@RequestParam(value="number") String number, @RequestParam(value="nama") String nama, @RequestParam(value="role") String role) {
        return ResponseEntity.ok(personService.createPerson(number, nama, role));
    }

    @GetMapping(path = "/{number}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getPerson(@PathVariable(value = "number") String number) {
        PersonLog person = personService.getPerson(number);
        if (person == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(person);
    }

    @PutMapping(path = "/", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updatePerson(@RequestBody PersonLog person, @PathVariable("number")String number) {
        return ResponseEntity.ok(personService.updatePerson(person, number));
    }

    @DeleteMapping(path = "/{number}", produces = {"application/json"})
    public ResponseEntity deletePerson(@PathVariable(value = "number") String number) {
        personService.deletePerson(number);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(path = "/{number}/join", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addGroup(@PathVariable(value = "number") String number, @RequestParam("kodeGroup")String kodeGroup){
        return ResponseEntity.ok(personService.addGroupHasJoin(number, kodeGroup));
    }

    @GetMapping(path = "/{number}/group",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<GroupLog>> getGroups(@PathVariable(value = "number") String number) {
        return ResponseEntity.ok(personService.getGroupJoin(number));
    }

    @GetMapping(path = "/{number}/task",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TaskLog>> getTasks(@PathVariable(value = "number") String number) {
        return ResponseEntity.ok(personService.getPersonalTask(number));
    }

}
