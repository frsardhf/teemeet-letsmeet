package adprog.teemeet.agenda.controller;

import adprog.teemeet.agenda.core.TaskDTO;
import adprog.teemeet.agenda.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

@Controller
@RequestMapping(path = "/agenda")
public class AgendaController {
    @Autowired
    private EventService eventService;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("")
    public String agenda(){
        LocalDateTime now = LocalDateTime.now();
        return "redirect:/agenda/calendar?month=" + now.getMonth().getValue() + "&year=" + now.getYear();
    }

    @GetMapping("/calendar")
    public String agendaLanding(Model model,
                          @RequestParam(value = "month") int month,
                          @RequestParam(value="year") int year){
        model.addAttribute("month", month);
        model.addAttribute("monthStr", Month.of(month));
        model.addAttribute("year", year);
        model.addAttribute("days", eventService.getDayList(year, month));
        return "agenda/agendaCalendar";
    }

    @GetMapping("/calendar/prev")
    public String agendaPrevMonth(@RequestParam(value = "month") int month,
                                  @RequestParam(value="year") int year){
        month -= 1;
        if (month == 0) {
            month = 12;
            year -= 1;
        }
        return "redirect:/agenda/calendar?month=" + month + "&year=" + year;
    }

    @GetMapping("/calendar/next")
    public String agendaNextMonth(@RequestParam(value = "month") int month,
                                  @RequestParam(value="year") int year){
        month += 1;
        if (month == 13) {
            month = 1;
            year += 1;
        }
        return "redirect:/agenda/calendar?month=" + month + "&year=" + year;
    }

    @GetMapping("/events")
    public String dailyEvents(Model model,
                                @RequestParam(value = "month") int month,
                                @RequestParam(value="year") int year,
                                @RequestParam(value="day") int day){
        model.addAttribute("month", month);
        model.addAttribute("monthStr", Month.of(month));
        model.addAttribute("year", year);
        model.addAttribute("day", day);
        model.addAttribute("events", eventService.getEventByYearMonthDay(year, month, day));
        return "agenda/agendaDailyEvents";
    }

    @GetMapping("/events/create")
    public String createEventForm(Model model,
                              @RequestParam(value = "month") int month,
                              @RequestParam(value="year") int year,
                              @RequestParam(value="day") int day){
        model.addAttribute("date", eventService.dateFormatter(year,month,day));
        return "agenda/createEvent";
    }

    @PostMapping("/events/create")
    public String createEventSubmit(HttpServletRequest request){
        String name = request.getParameter("name");
        String desc = request.getParameter("desc");
        String date = request.getParameter("date");
        String timeStart = request.getParameter("startTime");
        String timeEnd = request.getParameter("endTime");
        LocalDate ld = LocalDate.parse(date);
        eventService.createEvent(name,desc,date,timeStart,timeEnd);
        return "redirect:/agenda/events?month=" + ld.getMonthValue() + "&year=" + ld.getYear() + "&day=" + ld.getDayOfMonth();
    }

    @GetMapping("/events/{eventId}")
    public String viewEvent(Model model, @PathVariable(value = "eventId") Long eventId){
        model.addAttribute("event", eventService.getEventByEventId(eventId));
        TaskDTO[] taskDTOS = restTemplate.getForObject("http://localhost:8081/task/eventId/" + eventId, TaskDTO[].class);
        model.addAttribute("tasks", taskDTOS);
        return "agenda/viewEvent";
    }

    @PostMapping("/events/{eventId}")
    public String updateEvent(HttpServletRequest request,  @PathVariable(value = "eventId") Long eventId){
        String name = request.getParameter("name");
        String desc = request.getParameter("desc");
        String date = request.getParameter("date");
        String timeStart = request.getParameter("startTime");
        String timeEnd = request.getParameter("endTime");
        LocalDate ld = LocalDate.parse(date);
        eventService.updateEvent(eventId, name,desc,date,timeStart,timeEnd);
        return "redirect:/agenda/events/" + eventId;
    }
    //@GetMapping("/events/{id_event}")

    @GetMapping("/events/{eventId}/createTask")
    public String createTaskForAnEventForm(Model model, @PathVariable(value = "eventId") Long eventId){
        model.addAttribute("eventId",eventId);
        return "agenda/createTask";
    }

    @PostMapping("/events/{eventId}/createTask")
    public String createTaskForAnEventSubmit(HttpServletRequest request,
                            @PathVariable(value = "eventId") Long eventId
                                    /*
                            @RequestParam(value="idTask") Long idTask,
                            @RequestParam(value="judulTask") String judulTask,
                            @RequestParam(value="assignee") String assignee

                                     */
    ){
        String judulTask = request.getParameter("judulTask");
        String assignee = request.getParameter("assignee");
        HttpEntity<TaskDTO> requestEntity = new HttpEntity<>(new TaskDTO(judulTask,assignee,eventId));
        TaskDTO taskDTO = restTemplate.postForObject("http://localhost:8081/task?&idTask=&judulTask="+
                        judulTask +"&assignee="+
                        assignee + "&eventId=" + eventId,
                requestEntity, TaskDTO.class);
        return "redirect:/agenda/events/"+eventId;
    }


}