package adprog.teemeet.agenda.service;

import adprog.teemeet.agenda.core.DailyEvents;
import adprog.teemeet.agenda.model.Event;
import adprog.teemeet.agenda.repository.AgendaEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceImpl implements EventService{
    @Autowired
    AgendaEventRepository agendaEventRepository;

    @Autowired
    CalendarServiceImpl calendarService;

    @Override
    public Event createEvent(String name, String desc, String date, String timeStart, String timeEnd) {
        Event event = new Event(name,desc, date, timeStart,timeEnd);
        agendaEventRepository.save(event);
        return event;
    }

    @Override
    public Event getEventByEventId(Long eventId) {
        return agendaEventRepository.getEventByEventId(eventId);
    }


    @Override
    public List<Event> getEventByYearMonthDay(int year, int month, int day) {
        return agendaEventRepository.getByYearAndMonthAndDay(year, month, day);
    }

    @Override
    public List<DailyEvents> getDayList(int year, int month) {
        List<DailyEvents> res = new ArrayList<>();
        DailyEvents eventsPerDailyEvents;
        List<Event> events;
        for (int day :calendarService.getDaysinAMonth(year,month)) {
            if (day != 0){
                events = getEventByYearMonthDay(year, month, day);
                eventsPerDailyEvents = new DailyEvents(day, events);
                res.add(eventsPerDailyEvents);
            }
            else {
                res.add(new DailyEvents());
            }
        }
        return res;
    }

    @Override
    public LocalDate dateFormatter(int year, int month, int day){
        DecimalFormat formatter = new DecimalFormat("00");
        String dayFormatted = formatter.format(day);
        String monthFormatted = formatter.format(month);
        String dateString = year + "-" + monthFormatted + "-" + dayFormatted;
        return LocalDate.parse(dateString);
    }

    @Override
    public Event updateEvent(Long eventId, String name, String desc, String date, String timeStart, String timeEnd) {
        Event event = agendaEventRepository.getEventByEventId(eventId);
        event.setName(name);
        event.setDescription(desc);
        event.setDate(LocalDate.parse(date));
        event.setTimeStart(LocalTime.parse(timeStart));
        event.setTimeEnd(LocalTime.parse(timeEnd));
        agendaEventRepository.save(event);
        return event;
    }

}


