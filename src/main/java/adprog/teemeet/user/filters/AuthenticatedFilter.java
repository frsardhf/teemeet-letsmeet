package adprog.teemeet.user.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

public class AuthenticatedFilter extends GenericFilterBean {

    private String[] unauthenticatedURI = {"/", "/signin", "/signup", "/signup/success"} ;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;

        if (isAuthenticated()) {
            for (String uris : unauthenticatedURI) {
                if (uris.equals(servletRequest.getRequestURI())) {
                    String encodedRedirectURL = ((HttpServletResponse) response).encodeRedirectURL("/dashboard");

                    servletResponse.setStatus(HttpServletResponse.SC_TEMPORARY_REDIRECT);
                    servletResponse.setHeader("Location", encodedRedirectURL);

                    break;
                }
            }            
        }

        chain.doFilter(servletRequest, servletResponse);
    }
    
    private boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return false;
        }
        return authentication.isAuthenticated();
    }
}