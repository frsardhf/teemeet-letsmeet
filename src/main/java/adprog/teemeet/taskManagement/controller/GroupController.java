package adprog.teemeet.taskManagement.controller;
import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.service.GroupService;
import adprog.teemeet.taskManagement.service.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/group")
public class GroupController {
    @Autowired
    private GroupService groupService;


    @PostMapping(path = "/create",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createGroup(@RequestParam(value="nama") String nama, @RequestParam(value="kodeGroup") String kodeGroup,
                                      @RequestParam(value="user") String number) {
        return ResponseEntity.ok(groupService.createGroup(nama, kodeGroup, number));
    }



    @GetMapping(path = "/{kodeGroup}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getGroup(@PathVariable(value = "kodeGroup") String kodeGroup) {
        GroupLog group = groupService.getGroup(kodeGroup);
        if (group == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(group);
    }

    @PutMapping(path = "/{kodeGroup}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateGroup(@PathVariable(value = "kodeGroup") String kodeGroup, @RequestBody GroupLog group) {
        return ResponseEntity.ok(groupService.setNamaGroup(kodeGroup,group));
    }


    @DeleteMapping(path = "/{kodeGroup}", produces = {"application/json"})
    public ResponseEntity deletePerson(@PathVariable(value = "kodeGroup") String kodeGroup) {
        groupService.deleteGroup(kodeGroup);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/{kodeGroup}/groupTask",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<TaskLog>> getGroupTasks(@PathVariable(value = "kodeGroup") String kodeGroup) {
        return ResponseEntity.ok(groupService.getListTask(kodeGroup));
    }

    @GetMapping(path = "/{kodeGroup}/groupMember",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<PersonLog>> getTasks(@PathVariable(value = "kodeGroup") String kodeGroup) {
        return ResponseEntity.ok(groupService.getMember(kodeGroup));
    }

    @PostMapping(path = "/{kodeGroup}/add",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addMember(@PathVariable(value = "kodeGroup") String kodeGroup,@RequestBody PersonLog person) {
        return ResponseEntity.ok(groupService.addMember(kodeGroup, person));
    }

    @DeleteMapping(path = "/{kodeGroup}/delete", produces = {"application/json"})
    public ResponseEntity deleteMember(@PathVariable(value = "kodeGroup") String kodeGroup,@RequestBody PersonLog person) {
        groupService.removeMember(kodeGroup,person);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    
}
