package adprog.teemeet.agenda.core;

import lombok.Data;

@Data
public class TaskDTO {
    private Long idTask;

    private String judulTask;

    private int percent;

    private String status;

    private String assignne;

    private Long eventId;

    public TaskDTO(){}

    public TaskDTO(String judulTask, String assignne, long eventId){
        this.idTask = (long) 0;
        this.judulTask = judulTask;
        this.percent = 0;
        this.assignne = assignne;
        this.status = "";
        this.eventId = eventId;
    }


}



