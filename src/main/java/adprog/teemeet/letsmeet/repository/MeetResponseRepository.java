package adprog.teemeet.letsmeet.repository;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeetResponseRepository extends JpaRepository<LetsMeetResponse, Long>{
    List<LetsMeetResponse> findByEvent(LetsMeetEvent event);
}
