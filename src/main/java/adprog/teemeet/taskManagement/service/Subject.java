package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import org.springframework.stereotype.Service;


public interface Subject {
    GroupLog addMember(String kodeGroup, PersonLog person);
    void removeMember(String kodeGroup,PersonLog person);
    public void notifyTask(TaskLog task, GroupLog group);
}
