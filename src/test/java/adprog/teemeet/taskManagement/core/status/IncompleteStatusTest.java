package adprog.teemeet.taskManagement.core.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IncompleteStatusTest {
    private IncompleteStatus incompleteStatus;

    @BeforeEach
    void setUp() {
        incompleteStatus = new IncompleteStatus();
    }

    @Test
    void showStatus() {
        String status = incompleteStatus.showStatus();
        assertEquals("Incomplete", status);
    }
}