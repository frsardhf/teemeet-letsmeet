package adprog.teemeet.user.validators;

import java.util.Optional;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import adprog.teemeet.user.model.User;
import adprog.teemeet.user.model.UserDTO;
import adprog.teemeet.user.repository.UserRepository;

@Component
public class UserDTOValidator implements Validator {
 
    // common-validator library.
    private EmailValidator emailValidator = EmailValidator.getInstance();
 
    @Autowired
    private UserRepository userRepository;
 
    // The classes are supported by this validator.
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(UserDTO.class);
    }
 
    @Override
    public void validate(Object target, Errors errors) {
        UserDTO userDTO = (UserDTO) target;
 
        // Check the fields of userDTO
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty.appUserForm.userName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.appUserForm.email");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.appUserForm.password");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "matchingPassword", "NotEmpty.appUserForm.confirmPassword");
 
        if (!this.emailValidator.isValid(userDTO.getEmail())) {
            // Invalid email.
            errors.rejectValue("email", "Pattern.appUserForm.email");
        } else {
            Optional<User> user = userRepository.findByEmail(userDTO.getEmail());
            if (user.isPresent()) {
                // Email has been used by another account.
                errors.rejectValue("email", "Duplicate.appUserForm.email");
            }
        }
 
        if (!errors.hasFieldErrors("username")) {
            Optional<User> user = userRepository.findByUsername(userDTO.getUsername());
            if (user.isPresent()) {
                // Username is not available.
                errors.rejectValue("username", "Duplicate.appUserForm.userName");
            }
        }
 
        if (!errors.hasErrors()) {
            if (!userDTO.getMatchingPassword().equals(userDTO.getPassword())) {
                errors.rejectValue("matchingPassword", "Match.appUserForm.confirmPassword");
            }
        }
    }
 
}