package adprog.teemeet.user.service;

import java.text.MessageFormat;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import adprog.teemeet.user.model.User;
import adprog.teemeet.user.model.UserDTO;
import adprog.teemeet.user.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		final Optional<User> user = userRepository.findByUsername(username);

		if (user.isPresent()) {
			return user.get();
		} else {
			throw new UsernameNotFoundException(MessageFormat.format("User with username {0} cannot be found.", username));
		}

	}

	public User signUpUser(UserDTO userDTO) {

		final String encryptedPassword = bCryptPasswordEncoder.encode(userDTO.getPassword());
	
		User user = new User();
		user.setUsername(userDTO.getUsername());
		user.setEmail(userDTO.getEmail());
		user.setPassword(encryptedPassword);

		user.setEnabled(true);

		return userRepository.save(user);
	
	}
}