package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.model.LetsMeetResponse;

import java.util.List;

public interface MeetResponseService {
    LetsMeetResponse createMeetResponse(Long id,
                                        String username,
                                        List<String> selectedTime,
                                        LetsMeetResponse response);

    void deleteResponse(LetsMeetResponse response);

    Iterable<LetsMeetResponse> getListResponseByEvent(Long id);

    void deleteAllResponse(Long id);
}
