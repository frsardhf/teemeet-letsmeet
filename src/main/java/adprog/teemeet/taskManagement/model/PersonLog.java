package adprog.teemeet.taskManagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "personlog")
@Data
@NoArgsConstructor
public class PersonLog{

        @Id
        @Column(name = "number", updatable = false, nullable = false)
        private String number;

        @Column(name="nama")
        private String nama;

        @Column(name="role")
        private String role;

        @JsonIgnore
        @ManyToMany (cascade = CascadeType.ALL)
        @JoinTable(
                name = "has_joined",
                joinColumns = @JoinColumn(name = "person_id"),
                inverseJoinColumns = @JoinColumn(name = "group_id")
        )
        private List<GroupLog> groups;

        @JsonIgnore
        @ManyToMany (cascade = CascadeType.ALL)
        @JoinTable(
                name = "tasks",
                joinColumns = @JoinColumn(name = "person_id"),
                inverseJoinColumns = @JoinColumn(name= "task_id")
        )
        private List<TaskLog> tasks;

        public PersonLog(String number,String nama, String role) {
            this.number = number;
            this.nama = nama;
            this.role = role;
    }
}
