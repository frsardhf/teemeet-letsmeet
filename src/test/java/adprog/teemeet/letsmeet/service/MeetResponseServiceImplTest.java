package adprog.teemeet.letsmeet.service;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;
import adprog.teemeet.letsmeet.repository.MeetResponseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class MeetResponseServiceImplTest {
    @Mock
    private MeetResponseRepository meetResponseRepository;

    @Mock
    private MeetEventService meetEventService;

    @InjectMocks
    private MeetResponseServiceImpl meetResponseService;

    private LetsMeetEvent letsMeetEvent;
    private LetsMeetResponse letsMeetResponse;
    private LetsMeetResponse letsMeetResponse2;

    @BeforeEach
    public void setUp() {
        letsMeetEvent = new LetsMeetEvent();
        letsMeetEvent.setId(Long.parseLong("135135"));
        letsMeetEvent.setName("Group Project Microservice");
        letsMeetEvent.setDate(LocalDate.parse("2021-05-21"));
        String timePattern = "HH-mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        letsMeetEvent.setStartTime(LocalTime.parse("08-00", timeFormatter));
        letsMeetEvent.setEndTime(LocalTime.parse("13-00", timeFormatter));

        letsMeetResponse = new LetsMeetResponse();
        letsMeetResponse.setId(Long.parseLong("136136"));
        letsMeetResponse.setEvent(letsMeetEvent);
        letsMeetResponse.setUsername("James");
        List<String> selectedTime = Arrays.asList("15-00", "15-30");
        letsMeetResponse.setSelectedTime(selectedTime);
    }

    @Test
    public void testServiceCreateResponse(){
        lenient().when(meetResponseService
                .createMeetResponse(letsMeetResponse.getId(), letsMeetResponse.getUsername(),
                                    letsMeetResponse.getSelectedTime(), letsMeetResponse))
                .thenReturn(letsMeetResponse);
        LetsMeetResponse responseResult = meetResponseService
                .createMeetResponse(letsMeetResponse.getId(), letsMeetResponse.getUsername(),
                                    letsMeetResponse.getSelectedTime(), new LetsMeetResponse());
        assertEquals(letsMeetResponse.getSelectedTime(), responseResult.getSelectedTime());
    }

    @Test
    public void testServiceGetListResponseById(){
        List<LetsMeetResponse> allResponse = meetResponseRepository.findByEvent(letsMeetEvent);
        when(meetResponseService.getListResponseByEvent(letsMeetEvent.getId()))
                .thenReturn(allResponse);
        Iterable<LetsMeetResponse> allResponseResult = meetResponseService.getListResponseByEvent(Long.parseLong("135135"));
        Assertions.assertIterableEquals(allResponse, allResponseResult);
    }

    @Test
    public void testServiceDeleteResponse(){
        meetResponseService.deleteResponse(letsMeetResponse);
        lenient().when(meetResponseRepository.findById(letsMeetResponse.getId())).thenReturn(null);
        assertEquals(null, meetResponseRepository.findById(letsMeetResponse.getId()));
    }

    @Test
    public void testServiceDeleteAllResponse(){
        meetResponseService.deleteAllResponse(letsMeetResponse.getId());
        Iterable<LetsMeetResponse> deletedResponses = meetResponseService.getListResponseByEvent(letsMeetEvent.getId());
        lenient().when(meetResponseService.getListResponseByEvent(letsMeetEvent.getId())).thenReturn(Collections.emptyList());
        assertEquals(Collections.emptyList(), deletedResponses);
    }
}
