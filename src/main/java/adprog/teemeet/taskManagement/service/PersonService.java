package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;

public interface PersonService {
    PersonLog addGroupHasJoin(String number,String kodeGroup);

    Iterable<GroupLog> getGroupJoin(String number);
    Iterable<TaskLog> getPersonalTask(String number);
    PersonLog createPerson(String number, String nama, String role);
    PersonLog getPerson(String number);
    PersonLog updatePerson(PersonLog person, String number);
    void deletePerson(String number);
}
