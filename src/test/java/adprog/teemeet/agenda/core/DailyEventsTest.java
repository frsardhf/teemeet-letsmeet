package adprog.teemeet.agenda.core;

import adprog.teemeet.agenda.model.Event;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DailyEventsTest {

    private DailyEvents dailyEvents;
    private Event event1;
    private Event event2;

    @BeforeEach
    public void setUp() throws Exception {
        List<Event> events = new ArrayList<>();
        event1 = new Event("E1", "desc", "2020-10-10");
        event2 = new Event("E2", "desc", "2020-10-10", "10:00:00");
        events.add(event1);
        events.add(event2);
        this.dailyEvents = new DailyEvents(10, events);
    }

    @Test
    void testDayToString() throws Exception {
        String expected = "10\n\nE1\nE2";
        assertEquals(expected, dailyEvents.toString());
    }

    @Test
    void testConstructEmptyDay() throws Exception {
        assertEquals(0, new DailyEvents().getDay());
    }

    @Test
    void testGetEvents() throws Exception {
        assertEquals(event1, dailyEvents.getEvents().get(0));
        assertEquals(event2, dailyEvents.getEvents().get(1));
    }
}
