package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;

import java.util.List;

public interface GroupService {

    GroupLog createGroup(String nama, String kodeGroup, String number);
    GroupLog setNamaGroup(String kode, GroupLog groupLog);
    void deleteGroup(String kodeGroup);
    GroupLog getGroup(String kodeGroup);
    Iterable<TaskLog> getListTask(String kodeGroup);
    GroupLog addTaskGroup(String kodeGroup,TaskLog task);
    void deleteTaskGroup(GroupLog group,TaskLog task);
    Iterable<PersonLog> getMember(String kodeGroup);
    GroupLog addMember(String kodeGroup, PersonLog person);
    void removeMember(String kodeGroup,PersonLog person);
    //public void notifyTask(TaskLog task, GroupLog group);
}
