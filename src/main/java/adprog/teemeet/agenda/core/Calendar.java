package adprog.teemeet.agenda.core;

import java.util.ArrayList;
import java.util.List;

public class Calendar {
    /***************************************************************************
     *  Given the month, day, and year, return which day
     *  of the week it falls on according to the Gregorian calendar.
     *  For month, use 1 for January, 2 for February, and so forth.
     *  Returns 0 for Sunday, 1 for Monday, and so forth.
     ***************************************************************************/

    private static final int[] days = new int[]{
            0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };

    private static final Calendar instance = new Calendar();

    private Calendar() {
    }

    // Source: https://introcs.cs.princeton.edu/java/21function/Calendar.java.html
    public int firstDayofaMonth(int month, int year) {
        int y = year - (14 - month) / 12;
        int x = y + y / 4 - y / 100 + y / 400;
        int m = month + 12 * ((14 - month) / 12) - 2;
        return (1 + x + (31 * m) / 12) % 7;
    }

    private static boolean isLeapYear(int year) {
        if ((year % 4 == 0) && (year % 100 != 0)) return true;
        return year % 400 == 0;
    }

    public int getLastDay(int year, int month) {
        if (month == 2 && isLeapYear(year)) return 29;
        else return days[month];
    }

    public List<Integer> getDatesinAMonth(int year, int month) {
        List<Integer> dates = new ArrayList<>();

        int nOfDays = getLastDay(year, month);

        int startingDay = firstDayofaMonth(month, year);

        for (int i = 0; i < startingDay; i++) {
            dates.add(0);
        }

        for (int i = 1; i <= nOfDays; i++) {
            dates.add(i);
        }
        return dates;
    }

    public static Calendar getInstance() {
        return instance;
    }


}