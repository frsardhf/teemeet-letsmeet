package adprog.teemeet.user.validators;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import adprog.teemeet.user.model.User;
import adprog.teemeet.user.model.UserDTO;
import adprog.teemeet.user.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
public class UserDTOValidatorTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserDTOValidator userDTOValidator;

    private UserDTO userDTO;

    private Errors errors;

    @BeforeEach
    public void setUp() throws Exception {
        userDTO = new UserDTO();
        errors = new BeanPropertyBindingResult(userDTO, "userDTO");
    }

    @Test
    public void testValidatorOnlySupportsUserDTOClass() throws Exception {
        assertTrue(userDTOValidator.supports(UserDTO.class));
        assertFalse(userDTOValidator.supports(String.class));
    }

    @Test
    public void testValidUserDTO() throws Exception {
        userDTO.setUsername("spring");
        userDTO.setEmail("spring@spring.com");
        userDTO.setPassword("spring");
        userDTO.setMatchingPassword("spring");

        userDTOValidator.validate(userDTO, errors);

        assertFalse(errors.hasErrors());
    }

    @Test
    public void testRejectIfEmptyErrors() throws Exception {
        userDTOValidator.validate(userDTO, errors);
        List<FieldError> fieldErrors = errors.getFieldErrors();

        assertTrue(errors.hasErrors());
        assertEquals("NotEmpty.appUserForm.userName", fieldErrors.get(0).getCode());
        assertEquals("NotEmpty.appUserForm.email", fieldErrors.get(1).getCode());
        assertEquals("NotEmpty.appUserForm.password", fieldErrors.get(2).getCode());
        assertEquals("NotEmpty.appUserForm.confirmPassword", fieldErrors.get(3).getCode());
    }

    @Test
    public void testRejectIfWhitespaceErrors() throws Exception {
        userDTO.setUsername(" ");
        userDTO.setEmail(" ");
        userDTO.setPassword(" ");
        userDTO.setMatchingPassword(" ");

        userDTOValidator.validate(userDTO, errors);
        List<FieldError> fieldErrors = errors.getFieldErrors();

        assertTrue(errors.hasErrors());
        assertEquals("NotEmpty.appUserForm.userName", fieldErrors.get(0).getCode());
        assertEquals("NotEmpty.appUserForm.email", fieldErrors.get(1).getCode());
        assertEquals("NotEmpty.appUserForm.password", fieldErrors.get(2).getCode());
        assertEquals("NotEmpty.appUserForm.confirmPassword", fieldErrors.get(3).getCode());
    }

    @Test
    public void testInvalidEmail() throws Exception {
        userDTO.setUsername("spring");
        userDTO.setEmail("spring@spring");
        userDTO.setPassword("spring");
        userDTO.setMatchingPassword("spring");

        userDTOValidator.validate(userDTO, errors);
        List<FieldError> fieldErrors = errors.getFieldErrors();

        assertTrue(errors.hasErrors());
        assertEquals("Pattern.appUserForm.email", fieldErrors.get(0).getCode());
    }

    @Test
    public void testDuplicateEmail() throws Exception {
        userDTO.setUsername("spring");
        userDTO.setEmail("spring@spring.com");
        userDTO.setPassword("spring");
        userDTO.setMatchingPassword("spring");

        User user = new User();
        user.setUsername("username");
        user.setEmail("spring@spring.com");
        user.setPassword("spring");
        user.setEnabled(true);

        when(userRepository.findByEmail(userDTO.getEmail())).thenReturn(Optional.ofNullable(user));
        when(userRepository.findByUsername(userDTO.getUsername())).thenReturn(Optional.ofNullable(null));

        userDTOValidator.validate(userDTO, errors);
        List<FieldError> fieldErrors = errors.getFieldErrors();

        assertTrue(errors.hasErrors());
        assertEquals("Duplicate.appUserForm.email", fieldErrors.get(0).getCode());
    }

    @Test
    public void testDuplicateUsername() throws Exception {
        userDTO.setUsername("spring");
        userDTO.setEmail("spring@spring.com");
        userDTO.setPassword("spring");
        userDTO.setMatchingPassword("spring");

        User user = new User();
        user.setUsername("spring");
        user.setEmail("spring@spring.co.id");
        user.setPassword("spring");
        user.setEnabled(true);

        when(userRepository.findByEmail(userDTO.getEmail())).thenReturn(Optional.ofNullable(null));
        when(userRepository.findByUsername(userDTO.getUsername())).thenReturn(Optional.ofNullable(user));

        userDTOValidator.validate(userDTO, errors);
        List<FieldError> fieldErrors = errors.getFieldErrors();

        assertTrue(errors.hasErrors());
        assertEquals("Duplicate.appUserForm.userName", fieldErrors.get(0).getCode());
    }

    @Test
    public void testNotMatchingPassword() throws Exception {
        userDTO.setUsername("spring");
        userDTO.setEmail("spring@spring.com");
        userDTO.setPassword("spring");
        userDTO.setMatchingPassword("spring123");

        when(userRepository.findByEmail(userDTO.getEmail())).thenReturn(Optional.ofNullable(null));
        when(userRepository.findByUsername(userDTO.getUsername())).thenReturn(Optional.ofNullable(null));

        userDTOValidator.validate(userDTO, errors);
        List<FieldError> fieldErrors = errors.getFieldErrors();

        assertTrue(errors.hasErrors());
        assertEquals("Match.appUserForm.confirmPassword", fieldErrors.get(0).getCode());
    }

}
