package adprog.teemeet.agenda.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalendarTest {

    private Calendar calendar;

    @BeforeEach
    public void setUp() throws Exception {
        calendar = Calendar.getInstance();
    }

    @Test
    void testAgendaCalendarGetDatesInAMonth() throws Exception {
        List expected = new ArrayList(Arrays.asList(0,0,0,0));
        for (int i = 1; i <= 30; i ++){
            expected.add(i);
        }
        assertEquals(expected , calendar.getDatesinAMonth(2021, 4));
    }

    @Test
    void testAgendaCalendarGetDatesInAMonthLeapYear() throws Exception {
        List expected = new ArrayList(Arrays.asList(0,0,0,0,0,0));
        for (int i = 1; i <= 29; i ++){
            expected.add(i);
        }
        assertEquals(expected , calendar.getDatesinAMonth(2020, 2));
    }

    @Test
    void testAgendaCalendarGetDatesInAMonthLeapYearMultipleof400() throws Exception {
        List expected = new ArrayList(Arrays.asList(0,0));
        for (int i = 1; i <= 29; i ++){
            expected.add(i);
        }
        assertEquals(expected , calendar.getDatesinAMonth(2000, 2));
    }

    @Test
    void testAgendaCalendarGetDatesInFebruraryNotLeapYear() throws Exception {
        List expected = new ArrayList(Arrays.asList(0,0,0,0));
        for (int i = 1; i <= 28; i ++){
            expected.add(i);
        }
        assertEquals(expected , calendar.getDatesinAMonth(2001, 2));
    }
}
