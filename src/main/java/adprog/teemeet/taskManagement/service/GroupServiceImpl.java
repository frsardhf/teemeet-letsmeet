package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.model.TaskLog;
import adprog.teemeet.taskManagement.repository.GroupRepository;
import adprog.teemeet.taskManagement.repository.PersonRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GroupServiceImpl implements GroupService{
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired

    private GroupService groupService;

    @Autowired
    private PersonService personService;

    @Override
    public GroupLog createGroup(String nama, String kodeGroup, String number) {
        GroupLog group = new GroupLog(nama,kodeGroup);
        groupRepository.save(group);
        personService.addGroupHasJoin(number, kodeGroup);
        return group;
    }

    @Override
    public GroupLog getGroup(String kodeGroup) {
        return groupRepository.findByKodeGroup(kodeGroup);
    }

    @Override
    public GroupLog setNamaGroup(String nama, GroupLog groupLog) {
        String namaGroup = groupLog.getNama();
        groupLog.setNama(namaGroup);
        groupRepository.save(groupLog);
        return groupLog;
    }

    @Override
    public void deleteGroup(String kodeGroup) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        groupRepository.delete(group);
    }

    @Override
    public Iterable<TaskLog> getListTask(String kodeGroup) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        System.out.println(group.getGroup_tasks().size());
        List<TaskLog> result = group.getGroup_tasks();
        System.out.println(result.size());
        return group.getGroup_tasks();
    }

    @Override
    public GroupLog addTaskGroup(String kodeGroup, TaskLog task) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        List<TaskLog> tasks = group.getGroup_tasks();
        TaskLog tasklog = taskRepository.findByIdTask(task.getIdTask());
        tasks.add(tasklog);
        System.out.println("Sebelum "+group.getGroup_tasks().size());
        group.setGroup_tasks(tasks);
        task.setTask_group(group);
        groupRepository.save(group);
        taskRepository.save(task);
        System.out.println("Sesudah"+group.getGroup_tasks().size());
        System.out.println("Abis itu "+group.getGroup_tasks().size());
        return group;
    }

    @Override
    public void deleteTaskGroup(GroupLog group, TaskLog task) {
        List<TaskLog> tasks = group.getGroup_tasks();
        tasks.remove(task);
    }

    @Override
    public Iterable<PersonLog> getMember(String kodeGroup) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        return group.getMember();
    }

    @Override
    public GroupLog addMember(String kodeGroup,PersonLog person) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        List<PersonLog> member = group.getMember();
        member.add(person);
        group.setMember(member);
        personRepository.save(person);
        for(PersonLog orang : member){
            member.remove(orang);
        }
        return group;
    }

    @Override
    public void removeMember(String kodeGroup,PersonLog person) {
        GroupLog group = groupRepository.findByKodeGroup(kodeGroup);
        List<PersonLog> member = group.getMember();
        for(PersonLog orang : member){
            member.remove(orang);
        }
        group.setMember(member);
        groupRepository.save(group);
    }
    /*
    @Override
    public void notifyTask(TaskLog task,GroupLog group) {
        List<PersonLog> member = group.getMember();
        PersonServiceImpl personService = new PersonServiceImpl();
        for(PersonLog person : member){
            personService.updateTask(, person.getNumber(), 0, , );
        }
    }

     */
}
