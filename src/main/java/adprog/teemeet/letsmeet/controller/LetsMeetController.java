package adprog.teemeet.letsmeet.controller;


import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import adprog.teemeet.letsmeet.model.LetsMeetResponse;
import adprog.teemeet.letsmeet.service.MeetEventService;
import adprog.teemeet.letsmeet.service.MeetResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class LetsMeetController {

    @Autowired
    private MeetEventService meetEventService;

    @Autowired
    private MeetResponseService meetResponseService;

    @GetMapping("/letsmeet")
    public String letsMeetForm(Model model) {
        model.addAttribute("event", new LetsMeetEvent());
        return "letsmeet/letsMeet";
    }

    @PostMapping("/letsmeet")
    public String letsMeetSubmit(@ModelAttribute LetsMeetEvent event, Model model) {
        meetEventService.createMeetEvent(event);
        model.addAttribute("event", event);
        model.asMap().clear();
        return "redirect:/letsmeet/details/" + event.getId();
    }

    @GetMapping(path = "letsmeet/details/{id}", produces = {"application/json"})
    public String getMeetEvent(@PathVariable(value = "id") Long id, Model model) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        List<List<String>> timeslots = meetEventService.getPartitionedMeetEventTimeSlots(event);
        model.addAttribute("event", event);
        model.addAttribute("timeslots", timeslots);
        return "letsmeet/details";
    }

    @PostMapping(path = "letsmeet/update/{id}")
    public String updateMeetEvent(@PathVariable(value = "id") Long id,
                                  @RequestParam("newStartTime") String startTime,
                                  @RequestParam("newEndTime") String endTime,
                                  Model model) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        event = meetEventService.updateEvent(startTime, endTime, event);
        meetResponseService.deleteAllResponse(id);
        model.addAttribute("event", event);
        model.asMap().clear();
        return "redirect:/letsmeet/details/" + event.getId();
    }

    @PostMapping(path = "/letsmeet/details/{id}", produces = {"application/json"})
    public String letsMeetResponseSubmit(@PathVariable(value = "id") Long id,
                                         @RequestParam("timeChecked") List<String> idTimes,
                                         Authentication authentication,
                                         Model model) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        ArrayList<String> selectedTime = new ArrayList<>();
        if (idTimes != null) {
            for (String idTime : idTimes) {
                selectedTime.add(idTime);
            }
        }
        LetsMeetResponse response = new LetsMeetResponse();
        String username = authentication.getName();
        Iterable<LetsMeetResponse> responses = meetResponseService.getListResponseByEvent(id);
        for (LetsMeetResponse resp : responses) {
            if (resp.getUsername().equals(username)) {
                meetResponseService.deleteResponse(resp);
            }
        }
        response = meetResponseService.createMeetResponse(id, username, selectedTime, response);
        model.addAttribute("response", response);
        model.asMap().clear();
        return "redirect:/letsmeet/responses/" + event.getId();
    }

    @GetMapping(path = "letsmeet/responses/{id}", produces = {"application/json"})
    public String getMeetEventResponse(@PathVariable(value = "id") Long id,
                                       Model model) {
        LetsMeetEvent event = meetEventService.getMeetEventById(id);
        Iterable<LetsMeetResponse> responses = meetResponseService.getListResponseByEvent(id);
        model.addAttribute("meetevent", event);
        model.addAttribute("responses", responses);
        return "letsmeet/responses";
    }

    @ModelAttribute("hourList")
    public List<String> getHourList() {
        List<String> hourList = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            StringBuilder sb = new StringBuilder();
            if (i < 10)
                sb.append("0" + i + "-00");
            else
                sb.append(i + "-00");
            hourList.add(sb.toString());
        }
        return hourList;
    }
}
