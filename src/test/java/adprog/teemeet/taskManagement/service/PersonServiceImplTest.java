package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.GroupLog;
import adprog.teemeet.taskManagement.model.PersonLog;
import adprog.teemeet.taskManagement.repository.GroupRepository;
import adprog.teemeet.taskManagement.repository.PersonRepository;
import adprog.teemeet.taskManagement.repository.TaskRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.lenient;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class PersonServiceImplTest {

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    private GroupServiceImpl groupService;

    @InjectMocks
    private TaskServiceImpl taskService;

    @InjectMocks
    private PersonServiceImpl personService;

    private GroupLog group;
    private PersonLog person;

    @BeforeEach
    void setUp() {
        group = new GroupLog();
        group.setNama("ters");
        group.setKodeGroup("12345");

        person = new PersonLog();
        person.setNama("budi");
        person.setRole("leader");
        person.setNumber("1");
    }

    @Test
    void updateTask() {
    }

    @Test
    void addGroupHasJoin() {
    }

    @Test
    void getGroupHasJoin() {
    }

    @Test
    void getPersonalTask() {
    }

    @Test
    void createPerson() {
        lenient().when(personService.createPerson("1","budi","leader")).thenReturn(person);
    }

    @Test
    void getPerson() {
        lenient().when(personService.getPerson("1")).thenReturn(person);
        PersonLog resultPerson = personService.getPerson(person.getNumber());
        assertEquals(person.getNumber(),resultPerson.getNumber());
    }

    @Test
    void updatePerson() {
        /*
        lenient().when(personService.updatePerson(person,"boedi","leader")).thenReturn(person);
        assertEquals(person.getNama(),"boedi");

         */
    }

    @Test
    void deletePerson() {
        /*
>>>>>>> 1e1ce9797dd7e1ad0f9e55ef5fe2d8bfaa16fed6
        person = new PersonLog();
        person.setNama("ters");
        person.setNumber("12");
        person.setRole("leader");
<<<<<<< HEAD
        personService.createPerson(person);
=======

         */
        personService.createPerson("12","ters","leader");
        personService.deletePerson("12");
        lenient().when(personService.getPerson("12")).thenReturn(null);
        assertEquals(null,personService.getPerson("12"));
    }
}