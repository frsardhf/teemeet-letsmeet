package adprog.teemeet.agenda.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "event")
@Data
@NoArgsConstructor
public class Event {
    @Id
    @Column(name="event_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long eventId;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="time_start")
    private LocalTime timeStart;

    @Column(name="time_end")
    private LocalTime timeEnd;

    @Column(name="date")
    private LocalDate date;

    public Event(String name, String desc, String date, String timeStart, String timeEnd){
        this.name = name;
        this.description = desc;
        this.date = LocalDate.parse(date);
        this.timeStart = timeStart != null ? LocalTime.parse(timeStart) : null;
        this.timeEnd = timeStart != null ? LocalTime.parse(timeEnd) : null;
    }

    public Event(String name, String desc, String date, String time){
        this(name, desc, date, time, time);
    }

    public Event(String name, String desc, String date){
        this(name, desc, date, null, null);
    }

    @Override
    public String toString(){
        return name;
    }
}
