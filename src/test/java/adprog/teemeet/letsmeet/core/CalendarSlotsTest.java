package adprog.teemeet.letsmeet.core;

import adprog.teemeet.letsmeet.model.LetsMeetEvent;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalendarSlotsTest {
    private CalendarSlots calendarSlots;

    /*
    @Test
    void testGetTimeSlots() {
        calendarSlots = new CalendarSlots();
        Long id = Long.parseLong("1351351");
        String name = "AP Project";
        String date = "2021-05-16";
        String startTime = "07:00";
        String endTime = "12:00";
        String timePattern = "HH:mm";
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(timePattern);
        LetsMeetEvent event = new LetsMeetEvent();
        event.setId(id);
        event.setName(name);
        event.setDate(LocalDate.parse(date));
        event.setStartTime(LocalTime.parse(startTime, timeFormatter));
        event.setEndTime(LocalTime.parse(endTime, timeFormatter));

        int expected = 0;
        int startHour = event.getStartTime().getHour();
        int endHour = event.getEndTime().getHour();
        for (int i = startHour; i <= endHour; i++) {
            expected += 2;
        }
        ArrayList<String> list = calendarSlots.getTimeSlots(event);
        List<List<String>> partitioned = calendarSlots.getPartitionedTimeSlots(list);
        assertEquals(expected - 1, list.size());
        assertEquals("07:00", calendarSlots.getStartTime().toString());
        assertEquals("12:00", calendarSlots.getEndTime().toString());
        assertEquals("07-00", list.get(0));
        assertEquals("12-00", list.get(list.size() - 1));
        assertEquals((int) Math.ceil((double) list.size()/8), partitioned.size());
    }

     */
}
